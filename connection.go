// Connection provides a simplified interface to a AMQP RabbitMQ connection.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"context"
	amqp "github.com/rabbitmq/amqp091-go"
	"net"
	"sync"
	"time"
)

// defaultConnectionTimeout is the pre-heartbeat connection timeout. Following amqp this defaults to 30 seconds.
const defaultConnectionTimeout = 30 * time.Second

// defaultHeartbeat is the heartbeat interval. Following amqp this defaults to 10 seconds.
const defaultHeartbeat = 10 * time.Second

// defaultLocale is the locale to use. Following amqp this defaults to "en_US".
const defaultLocale = "en_US"

// maxDialAttempts is the maximum number of attempts to make when dialing a connection.
const maxDialAttempts = 10

// Printer is the interface satisfied by the Printf method.
type Printer interface {
	Printf(format string, v ...interface{})
}

// Connection represents a connection to RabbitMQ.
type Connection struct {
	url      string           // The connection url
	l        Printer          // An optional logger
	m        sync.Mutex       // Mutex controlling access to the following
	isClosed bool             // Are we closed?
	closeErr error            // The error on close (if any)
	conn     *amqp.Connection // The current connection
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// dialWithContext attempts to dial a connection to the RabbitMQ server.
func dialWithContext(ctx context.Context, network string, addr string) (net.Conn, error) {
	// Dial using the context
	conn, err := (&net.Dialer{}).DialContext(ctx, network, addr)
	if err != nil {
		return nil, err
	}
	// Heartbeat hasn't started yet, don't stall forever on a dead server
	err = conn.SetDeadline(time.Now().Add(defaultConnectionTimeout))
	if err != nil {
		return nil, err
	}
	// Return the connection
	return conn, nil
}

// createDialFunc returns a dial function. This will use exponential backoff.
func createDialFunc(ctx context.Context) func(network string, addr string) (net.Conn, error) {
	return func(network string, addr string) (net.Conn, error) {
		// Loop with exponential back-off
		wait := time.Second
		attempt := 1
		for {
			// Attempt to dial a connection
			conn, err := dialWithContext(ctx, network, addr)
			// Any luck?
			if err == nil {
				return conn, nil
			} else if err == context.Canceled || err == context.DeadlineExceeded {
				return nil, err
			}
			// Should we abandon our attempts?
			if attempt == maxDialAttempts {
				return nil, err
			}
			// Wait
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			case <-time.After(wait):
				attempt++
			}
			// Double the wait time, if appropriate, and retry
			if wait < time.Minute {
				wait = 2 * wait
			}
		}
	}
}

// dialAMQPConnection dials a new AMQP connection to RabbitMQ.
func dialAMQPConnection(ctx context.Context, url string) (*amqp.Connection, error) {
	// Create the config
	config := amqp.Config{
		Heartbeat: defaultHeartbeat,
		Locale:    defaultLocale,
		Dial:      createDialFunc(ctx),
	}
	// Dial a connection
	return amqp.DialConfig(url, config)
}

// log logs a message to l.
func log(l Printer, format string, args ...interface{}) {
	if l != nil {
		l.Printf(format, args...)
	}
}

// logThrottling watches the channel blockingC for connection throttling, logging this info to l.
func logThrottling(c *Connection, blockingC <-chan amqp.Blocking) {
	for b := range blockingC {
		if b.Active {
			log(c.l, "Connection throttling enabled: %s", b.Reason)
		} else {
			log(c.l, "Connection throttling disabled")
		}
	}
}

// watchForConnectionClose watches the channel closeC for the connection closing.
func watchForConnectionClose(c *Connection, closeC <-chan *amqp.Error) {
	// Wait for the connection to close
	e, ok := <-closeC
	if ok {
		log(c.l, "Connection dropped: %v", e)
	}
	// Remove the dead connection
	c.m.Lock()
	defer c.m.Unlock()
	c.conn = nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Connect returns a connection to the RabbitMQ server at given URL.
func Connect(ctx context.Context, url string, l Printer) (*Connection, error) {
	// Sanity check
	_, err := amqp.ParseURI(url)
	if err != nil {
		return nil, ErrInvalidURL
	}
	// Create a connection object
	c := &Connection{
		url: url,
		l:   l,
	}
	// Establish a connection
	c.m.Lock()
	defer c.m.Unlock()
	if err := c.createConnectionNoLock(ctx); err != nil {
		return nil, err
	}
	return c, nil
}

// Close closes the connection.
func (c *Connection) Close() error {
	// Sanity check
	if c == nil {
		return nil
	}
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Is there anything to do?
	if !c.isClosed {
		c.isClosed = true
		if c.conn != nil {
			c.closeErr = c.conn.Close()
			// Provide some logging
			if c.closeErr != nil {
				log(c.l, "Connection closed with error: %v", c.closeErr)
			} else {
				log(c.l, "Connection closed")
			}
		}
	}
	// Return the close error (if any)
	return c.closeErr
}

// CreatePublisher returns a new publisher, using this connection.
func (c *Connection) CreatePublisher() (*Publisher, error) {
	// Sanity check
	if c == nil {
		return nil, ErrUninitialisedConnection
	}
	// Are we closed?
	c.m.Lock()
	defer c.m.Unlock()
	if c.isClosed {
		return nil, ErrConnectionClosed
	}
	// Return a new publisher
	return newPublisher(c), nil
}

// QueueDelete deletes the named queue, along with any messages in the queue, returning the number of messages deleted. When ifUnused is true, the queue will not be deleted if there are any consumers on the queue. If there are consumers, an error will be returned. When ifEmpty is true, the queue will not be deleted if there are any messages remaining on the queue. If there are messages, an error will be returned.
func (c *Connection) QueueDelete(ctx context.Context, queue string, ifUnused bool, ifEmpty bool) (int, error) {
	// Sanity check
	if c == nil {
		return 0, ErrUninitialisedConnection
	} else if len(queue) == 0 {
		return 0, ErrEmptyQueueName
	}
	// Create a new channel
	channel, err := c.newChannel(ctx)
	if err != nil {
		return 0, err
	}
	defer channel.Close()
	// Delete the queue
	n, err := channel.QueueDelete(queue, ifUnused, ifEmpty, false)
	// Ignore "queue not found" errors
	if isQueueNotFoundError(err) {
		return 0, nil
	}
	return n, err
}

// QueueClear deletes all messages from the named queue, returning the number of messages deleted. Messages that have been delivered but not yet acknowledged will not be deleted.
func (c *Connection) QueueClear(ctx context.Context, queue string) (int, error) {
	// Sanity check
	if c == nil {
		return 0, ErrUninitialisedConnection
	} else if len(queue) == 0 {
		return 0, ErrEmptyQueueName
	}
	// Create a new channel
	channel, err := c.newChannel(ctx)
	if err != nil {
		return 0, err
	}
	defer channel.Close()
	// Purge the queue
	n, err := channel.QueuePurge(queue, false)
	// Ignore "queue not found" errors
	if isQueueNotFoundError(err) {
		return 0, nil
	}
	return n, err
}

// QueueLen returns the number of messages in the given queue.
func (c *Connection) QueueLen(ctx context.Context, queue string) (int, error) {
	// Sanity check
	if c == nil {
		return 0, ErrUninitialisedConnection
	} else if len(queue) == 0 {
		return 0, ErrEmptyQueueName
	}
	// Create a new channel
	channel, err := c.newChannel(ctx)
	if err != nil {
		return 0, err
	}
	defer channel.Close()
	// Inspect the queue
	q, err := channel.QueueInspect(queue)
	if err != nil {
		// Ignore "queue not found" errors
		if isQueueNotFoundError(err) {
			err = nil
		}
		return 0, err
	}
	return q.Messages, nil
}

// QueueConsume returns a channel containing messages from the given queue using this connection. It also returns a cancel function. When the caller is finished with the channel, the cancel function must be called, otherwise resources may leak.
func (c *Connection) QueueConsume(queue string) (<-chan *Message, func(), error) {
	// Sanity check
	if c == nil {
		return nil, nil, ErrUninitialisedConnection
	} else if len(queue) == 0 {
		return nil, nil, ErrEmptyQueueName
	}
	// Are we closed?
	c.m.Lock()
	defer c.m.Unlock()
	if c.isClosed {
		return nil, nil, ErrConnectionClosed
	}
	// Create a new channel and consumer
	channel := newChannel(c, c.l)
	d, cancel, err := channel.Consume(queue)
	if err != nil {
		channel.Close()
		return nil, nil, err
	}
	// Return the channel and wrapped cancel function
	return d, func() {
		cancel()
		channel.Close()
	}, nil
}

// createConnectionNoLock builds a new AMQP connection to the RabbitMQ server. This assumes that a lock is held on the mutex, and that no connection currently exists.
func (c *Connection) createConnectionNoLock(ctx context.Context) error {
	// Dial a connection
	conn, err := dialAMQPConnection(ctx, c.url)
	if err != nil {
		return err
	}
	c.conn = conn
	// Provide some logging
	log(c.l, "Connection established")
	// Start the background go-routines running
	go logThrottling(c, conn.NotifyBlocked(make(chan amqp.Blocking)))
	go watchForConnectionClose(c, conn.NotifyClose(make(chan *amqp.Error)))
	return nil
}

// getConnection returns an active AMQP connection to the RabbitMQ server, or an error if the connection has been closed by the user.
func (c *Connection) getConnection(ctx context.Context) (*amqp.Connection, error) {
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if c.isClosed {
		return nil, ErrConnectionClosed
	}
	// Is there already an open connection? If not, build a new connection.
	if c.conn == nil {
		if err := c.createConnectionNoLock(ctx); err != nil {
			return nil, err
		}
	}
	return c.conn, nil
}

// newChannel returns a new channel over the AMQP connection.
func (c *Connection) newChannel(ctx context.Context) (*amqp.Channel, error) {
	conn, err := c.getConnection(ctx)
	if err != nil {
		return nil, err
	}
	return conn.Channel()
}
