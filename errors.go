// Errors describes common errors for RabbitMQ.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	amqp "github.com/rabbitmq/amqp091-go"
)

// rabbitmqError implements an error.
type rabbitmqError int

// Error codes
const (
	ErrInvalidURL = rabbitmqError(iota)
	ErrUninitialisedConnection
	ErrConnectionClosed
	ErrChannelClosed
	ErrPublisherClosed
	ErrUninitialisedPublisher
	ErrFlowDisabled
	ErrFailedToAckPublishing
	ErrFailedToAcceptPublishing
	ErrEmptyQueueName
	ErrIllegalQos
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isQueueNotFoundError returns true if err looks like a "queue not found" error from RabbitMQ.
func isQueueNotFoundError(e error) bool {
	// Check if we have the correct concrete type
	err, ok := e.(*amqp.Error)
	if !ok {
		return false
	}
	// Check if we have the right error code and a plausible-looking reason
	if err.Code != 404 || err.Reason[:21] != "NOT_FOUND - no queue " {
		return false
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// rabbitmqError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e rabbitmqError) Error() string {
	switch e {
	case ErrInvalidURL:
		return "invalid URL"
	case ErrUninitialisedConnection:
		return "uninitialised connection"
	case ErrConnectionClosed:
		return "connection closed"
	case ErrChannelClosed:
		return "channel closed"
	case ErrPublisherClosed:
		return "publisher closed"
	case ErrUninitialisedPublisher:
		return "uninitialised publisher"
	case ErrFlowDisabled:
		return "flow temporarily disabled by server"
	case ErrFailedToAckPublishing:
		return "server failed to acknowledge publishing"
	case ErrFailedToAcceptPublishing:
		return "server unable to accept publishing"
	case ErrEmptyQueueName:
		return "illegal empty queue name"
	case ErrIllegalQos:
		return "illegal value for quality of service"
	default:
		return "unknown error"
	}
}

// Is return true iff target is equal to e.
func (e rabbitmqError) Is(target error) bool {
	ee, ok := target.(rabbitmqError)
	return ok && ee == e
}
