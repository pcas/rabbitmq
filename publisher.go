// Publisher provides a simplified interface to a AMQP RabbitMQ publisher.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"bitbucket.org/pcastools/pool"
	"context"
	amqp "github.com/rabbitmq/amqp091-go"
	"sync"
	"time"
)

// DeliveryMode. Transient means higher throughput but messages will not be
// restored on broker restart. The delivery mode of publishings is unrelated
// to the durability of the queues they reside on. Transient messages will
// not be restored to durable queues, persistent messages will be restored to
// durable queues and lost on non-durable queues during server restart.
//
// This remains typed as uint8 to match Publishing.DeliveryMode. Other
// delivery modes specific to custom queue implementations are not enumerated
// here.
const (
	Transient  uint8 = amqp.Transient
	Persistent uint8 = amqp.Persistent
)

// defaultPoolLifetime is the lifetime of channels in the pool.
const defaultPoolLifetime = 10 * time.Second

// maxPublishAttempts is the maximum number of times a publish is attempted due to flow errors.
const maxPublishAttempts = 20

// Publisher is used to publish messages to queues.
type Publisher struct {
	l        Printer      // An optional logger
	pool     *pool.Pool   // The channel pool
	m        sync.RWMutex // Controls access to the following
	isClosed bool         // Are we closed?
	closeErr error        // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// newPublisher creates a new publisher for the given connection.
func newPublisher(c *Connection) *Publisher {
	return &Publisher{
		l: c.l,
		pool: pool.New(
			defaultPoolLifetime,
			func() (interface{}, error) {
				return newChannel(c, c.l), nil
			},
			func(x interface{}) {
				x.(*channel).Close()
			},
		),
	}
}

/////////////////////////////////////////////////////////////////////////
// Publisher functions
/////////////////////////////////////////////////////////////////////////

// Close closes the publisher.
func (p *Publisher) Close() error {
	// Sanity check
	if p == nil {
		return nil
	}
	// Acquire a write lock
	p.m.Lock()
	defer p.m.Unlock()
	// Is there anything to do?
	if !p.isClosed {
		p.isClosed = true
		p.closeErr = p.pool.Close()
		// Provide some logging
		if p.closeErr != nil {
			log(p.l, "Publisher closed with error: %v", p.closeErr)
		} else {
			log(p.l, "Publisher closed")
		}
	}
	// Return the close error (if any)
	return p.closeErr
}

// Publish publishes a message to the given queue.
func (p *Publisher) Publish(ctx context.Context, queue string, msg []byte) error {
	// Sanity check
	if p == nil {
		return ErrUninitialisedPublisher
	} else if len(queue) == 0 {
		return ErrEmptyQueueName
	}
	// We retry on flow error
	wait := 10 * time.Millisecond
	attempt := 1
	for {
		// Try to publish
		err := p.tryToPublish(ctx, queue, msg)
		// Was this a success? Should we abandon our attempts?
		if err != ErrFlowDisabled || attempt == maxPublishAttempts {
			return err
		}
		// Wait
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(wait):
			attempt++
		}
		// Double the wait time, if appropriate, and retry
		if wait < time.Minute {
			wait = 2 * wait
		}
	}
}

// tryToPublish attempts to publish a message to the given queue.
func (p *Publisher) tryToPublish(ctx context.Context, queue string, msg []byte) error {
	// Acquire a read lock
	p.m.RLock()
	defer p.m.RUnlock()
	// Are we closed?
	if p.isClosed {
		return ErrPublisherClosed
	}
	// Fetch a channel
	channel, err := p.getChannel()
	if err != nil {
		return err
	}
	defer p.reuseChannel(channel)
	// Publish the message
	return channel.Publish(ctx, queue, msg)
}

// getChannel returns a channel from the pool, or an error if the publisher or connection has been closed by the user.
func (p *Publisher) getChannel() (*channel, error) {
	c, err := p.pool.Get()
	if err != nil {
		return nil, err
	}
	return c.(*channel), nil
}

// reuseChannel places the given channel back in the pool.
func (p *Publisher) reuseChannel(c *channel) {
	p.pool.Put(c)
}
