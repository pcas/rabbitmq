// Channel provides a simplified interface to a AMQP RabbitMQ channel.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"bitbucket.org/pcastools/ulid"
	"context"
	amqp "github.com/rabbitmq/amqp091-go"
	"sync"
	"time"
)

// channel wraps an AMQP channel with associated connection.
type channel struct {
	conn     *Connection              // The underlying connection
	l        Printer                  // An optional logger
	m        sync.Mutex               // Controls access to the following
	queues   map[string]bool          // The registered queues
	qos      int                      // The current qos value
	isClosed bool                     // Are we closed?
	closeErr error                    // The error on close (if any)
	channel  *amqp.Channel            // The underlying channel
	flow     bool                     // Are we in a flow state?
	notifyC  <-chan amqp.Confirmation // The confirmation channel
	returnC  <-chan amqp.Return       // The return channel
}

// Message represents a message.
type Message struct {
	m amqp.Delivery // The underlying message
}

// Acknowledger is the interface satisfied by something that can positively or negatively acknowledge.
type Acknowledger interface {
	Ack() error
	Nack() error
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// watchForFlow watches flowC for flow control.
func watchForFlow(c *channel, channel *amqp.Channel, flowC <-chan bool) {
	for flow := range flowC {
		// Provide some logging
		if flow {
			log(c.l, "Flow resumed")
		} else {
			log(c.l, "Flow paused")
		}
		// Record the change in flow state
		c.m.Lock()
		if c.channel == channel {
			c.flow = flow
		}
		c.m.Unlock()
	}
}

// watchForChannelClose watches closeC for the channel closing.
func watchForChannelClose(c *channel, channel *amqp.Channel, closeC <-chan *amqp.Error) {
	// Wait for the connection to close
	e, ok := <-closeC
	if ok {
		log(c.l, "Channel dropped: %v", e)
	}
	// Aquire a lock and remove the dead channel
	c.m.Lock()
	defer c.m.Unlock()
	if c.channel == channel {
		c.channel = nil
	}
}

/////////////////////////////////////////////////////////////////////////
// acknowledger functions
/////////////////////////////////////////////////////////////////////////

// acknowledger is an amqp.Acknowledger and delivery tag.
type acknowledger struct {
	a   amqp.Acknowledger
	tag uint64
}

// acknowledger satisfies Acknowledger
var _ Acknowledger = acknowledger{}

// Ack sends a positive acknowledgement.
func (a acknowledger) Ack() error {
	return a.a.Ack(a.tag, false)
}

// Nack sends a negative acknowledgement.
func (a acknowledger) Nack() error {
	return a.a.Nack(a.tag, false, true)
}

/////////////////////////////////////////////////////////////////////////
// Message functions
/////////////////////////////////////////////////////////////////////////

// Body returns the message body.
func (m *Message) Body() []byte {
	if m == nil {
		return nil
	}
	return m.m.Body
}

// Timestamp returns the timestamp of the message, if set.
func (m *Message) Timestamp() time.Time {
	if m == nil {
		return time.Time{}
	}
	return m.m.Timestamp
}

// String returns the message body as a string.
func (m *Message) String() string {
	return string(m.Body())
}

// Ack positively acknowledges the message.
func (m *Message) Ack() error {
	if m == nil {
		return nil
	}
	return m.m.Ack(false)
}

// Nack negatively acknowledges the message, and requeues it.
func (m *Message) Nack() error {
	if m == nil {
		return nil
	}
	return m.m.Nack(false, true)
}

// Acknowledger returns an acknowledger for this message.
func (m *Message) Acknowledger() Acknowledger {
	return acknowledger{
		a:   m.m.Acknowledger,
		tag: m.m.DeliveryTag,
	}
}

/////////////////////////////////////////////////////////////////////////
// channel functions
/////////////////////////////////////////////////////////////////////////

// newChannel returns a new channel wrapping the given connection.
func newChannel(conn *Connection, l Printer) *channel {
	return &channel{
		conn: conn,
		l:    l,
	}
}

// Close closes the channel.
func (c *channel) Close() error {
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Is there anything to do?
	if !c.isClosed {
		c.isClosed = true
		if c.channel != nil {
			c.closeErr = c.channel.Close()
			// Provide some logging
			if c.closeErr != nil {
				log(c.l, "Channel closed with error: %v", c.closeErr)
			} else {
				log(c.l, "Channel closed")
			}
		}
	}
	// Return the close error (if any)
	return c.closeErr
}

// Qos sets the quality of service. The argument is the prefetch count, that is, the number of messages to fetch from the server before requiring delivery acknowledgements. To get round-robin behavior between consumers consuming from the same queue on different connections, set the prefetch count to 1, and the next available message on the server will be delivered to the next available consumer. The RabbitMQ documentation suggests that if your consumer work time is reasonably consistent and not much greater than two times your network round trip time, you will see significant throughput improvements starting with a prefetch count of 2 or slightly greater.
func (c *channel) Qos(n int) error {
	// Sanity check
	if n < 0 {
		return ErrIllegalQos
	}
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if c.isClosed {
		return ErrChannelClosed
	}
	// Make a note of the quality of service value
	c.qos = n
	// If necessary, update the channel
	if c.channel != nil {
		c.channel.Qos(n, 0, false) // Ignore any error
	}
	return nil
}

// createConsumer attempts to register a consumer for the given queue. Also returns a cancel function for the consumer.
func (c *channel) createConsumer(queue string) (<-chan amqp.Delivery, func(), error) {
	// Generate a consumer ID
	u, err := ulid.New()
	if err != nil {
		return nil, nil, err
	}
	consumerID := u.String()
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Create a context with timeout
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Get the channel
	channel, err := c.getChannelNoLock(ctx)
	if err != nil {
		return nil, nil, err
	}
	// If necessary, register the queue
	if c.queues == nil {
		c.queues = make(map[string]bool)
	}
	if !c.queues[queue] {
		// We need to register the queue
		if _, err = channel.QueueDeclare(queue, true, false, false, false, nil); err != nil {
			return nil, nil, err
		}
		c.queues[queue] = true
	}
	// Create the consumer
	d, err := channel.Consume(queue, consumerID, false, false, false, false, nil)
	if err != nil {
		return nil, nil, err
	}
	// Return the channel and cancel function
	return d, func() {
		channel.Cancel(consumerID, false) // Ignore any error
	}, nil
}

// consumerWorker feeds messages from the given queue on c to the channel d. Close doneC to shut down the worker. Intended to be run in its own goroutine.
func (c *channel) consumerWorker(queue string, d chan<- *Message, doneC <-chan struct{}) {
	defer close(d)
	for {
		// Create a consumer
		locald, localCancel, err := c.createConsumer(queue)
		if err != nil {
			if err != ErrChannelClosed {
				log(c.l, "error creating consumer: %v", err)
			}
			return
		}
		// Pass along messages
		ok := true
		for ok {
			var m amqp.Delivery
			select {
			case <-doneC: // The user called the cancel function
				localCancel()
				return
			case m, ok = <-locald:
				if !ok {
					localCancel()
				} else {
					select {
					case <-doneC: // The user called the cancel function
						localCancel()
						return
					case d <- &Message{m: m}:
					}
				}
			}
		}
	}
}

// Consume returns a channel containing messages from the given queue, together with a cancel function. When the caller is finished with the channel, the cancel function must be called, otherwise resources may leak.
func (c *channel) Consume(queue string) (<-chan *Message, func(), error) {
	// Sanity check
	if len(queue) == 0 {
		return nil, nil, ErrEmptyQueueName
	}
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Are we closed?
	if c.isClosed {
		return nil, nil, ErrChannelClosed
	}
	// Create the communication channels
	d := make(chan *Message)
	doneC := make(chan struct{})
	// Start a background go-routine
	go c.consumerWorker(queue, d, doneC)
	// Return the channel and cancel function
	return d, func() {
		close(doneC)
	}, nil
}

// Publish publishes a message to the channel. All publishing attempts wait for acknowledgement, therefore only one Publish can take place at a time per channel. For high-throughput, use multiple channels. If the named queue does not exist, it will be created as a durable AMQP queue bound to the empty exchange "" with autoDelete disabled.
func (c *channel) Publish(ctx context.Context, queue string, msg []byte) error {
	// Sanity check
	if len(queue) == 0 {
		return ErrEmptyQueueName
	}
	// Acquire a lock
	c.m.Lock()
	defer c.m.Unlock()
	// Get the channel
	channel, err := c.getChannelNoLock(ctx)
	if err != nil {
		return err
	}
	// Is flow disabled?
	if !c.flow {
		return ErrFlowDisabled
	}
	// If necessary, register the queue
	if c.queues == nil {
		c.queues = make(map[string]bool)
	}
	if !c.queues[queue] {
		// We need to register the queue
		if _, err = channel.QueueDeclare(queue, true, false, false, false, nil); err != nil {
			return err
		}
		c.queues[queue] = true
	}
	// Publish the message
	err = channel.PublishWithContext(ctx, "", queue, true, false, amqp.Publishing{
		ContentType:  "text/plain",
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		Body:         msg,
	})
	if err != nil {
		return err
	}
	// Wait for acknowledgement or return
	select {
	case <-ctx.Done():
		// The context fired before we got a response -- drop the channel
		go func() { channel.Close() }()
		log(c.l, "Channel dropped: timeout waiting for acknowledgment")
		c.channel = nil
		return ctx.Err()
	case n := <-c.notifyC:
		if !n.Ack {
			log(c.l, "Server failed to acknowledge publishing")
			return ErrFailedToAckPublishing
		}
	case r := <-c.returnC:
		if len(r.ReplyText) != 0 {
			log(c.l, "Server unable to accept publishing: %s", r.ReplyText)
		} else {
			log(c.l, "Server unable to accept publishing")
		}
		return ErrFailedToAcceptPublishing
	}
	return nil
}

// newChannelNoLock builds a new AMQP channel to the RabbitMQ server over the wrapped connection. Assumes that a lock is held on the mutex, and that no channel currently exists.
func (c *channel) newChannelNoLock(ctx context.Context) error {
	// Fetch a new channel
	channel, err := c.conn.newChannel(ctx)
	if err != nil {
		return err
	}
	c.channel = channel
	c.flow = true
	// Provide some logging
	log(c.l, "Channel created")
	// Start the background go-routines running
	go watchForFlow(c, channel, channel.NotifyFlow(make(chan bool)))
	go watchForChannelClose(c, channel, channel.NotifyClose(make(chan *amqp.Error)))
	// Register and record the channels
	c.notifyC = channel.NotifyPublish(make(chan amqp.Confirmation, 1))
	c.returnC = channel.NotifyReturn(make(chan amqp.Return, 1))
	// Enable confirmations
	channel.Confirm(false)
	return nil
}

// getChannelNoLock returns an active AMQP channel to the RabbitMQ server over the wrapped connection, or an error if the channel or connection has been closed by the user. Assumes that a lock is held on the mutex.
func (c *channel) getChannelNoLock(ctx context.Context) (*amqp.Channel, error) {
	// Are we closed?
	if c.isClosed {
		return nil, ErrChannelClosed
	}
	// Is there already an open channel? If not, create one
	if c.channel == nil {
		if err := c.newChannelNoLock(ctx); err != nil {
			return nil, err
		}
		// This is a new channel, so we will need to re-register our queues and
		// set the quality of service value
		c.queues = nil
		c.channel.Qos(c.qos, 0, false) // Ignore any error
	}
	return c.channel, nil
}
