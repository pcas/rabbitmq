module bitbucket.org/pcas/rabbitmq

go 1.16

require (
	bitbucket.org/pcastools/pool v1.0.4
	bitbucket.org/pcastools/ulid v0.1.6
	github.com/rabbitmq/amqp091-go v1.8.0
	github.com/stretchr/testify v1.8.0
)
