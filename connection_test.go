// connection_test provides tests for the rabbitmq package.

//go:build integration
// +build integration

/*
This test requires RabbitMQ to be running and listening on localhost:5672.

To run, do:
go test -tags=integration
*/

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rabbitmq

import (
	"context"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

// randomName returns a random ASCII string of length 32
func randomName() string {
	rand.Seed(time.Now().Unix())
	chars := []byte("abcdefghijklmnopqrstuvwxyz0123456789-_")
	vals := make([]byte, 0, 32)
	for i := 0; i < 32; i++ {
		j := rand.Intn(len(chars))
		vals = append(vals, chars[j])
	}
	return string(vals)
}

// readFrom reads from C
func readFrom(ctx context.Context, C <-chan *Message) ([]byte, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case m := <-C:
		m.Ack()
		return m.Body(), nil
	}
}

func TestAll(t *testing.T) {
	// Create a context for the tests
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Connect to RabbitMQ
	conn, err := Connect(ctx, "amqp://guest:guest@localhost:5672/", nil)
	require.NoError(t, err)
	defer conn.Close()
	// Create a publisher
	pub, err := conn.CreatePublisher()
	require.NoError(t, err)
	defer pub.Close()
	// Make a queue name
	q := randomName()
	// Check that there are no messages in the queue
	n, err := conn.QueueLen(ctx, q)
	require.NoError(t, err)
	require.Zero(t, n)
	// Publish some messages
	messages := []string{
		"one",
		"two",
		"three",
	}
	for _, msg := range messages {
		err := pub.Publish(ctx, q, []byte(msg))
		require.NoError(t, err)
	}
	// There should be three messages
	n, err = conn.QueueLen(ctx, q)
	require.NoError(t, err)
	require.Equal(t, 3, n)
	// Create a consumer
	C, thisCancel, err := conn.QueueConsume(q)
	require.NoError(t, err)
	defer thisCancel()
	// Consume the messages
	for _, expected := range messages {
		b, err := readFrom(ctx, C)
		require.NoError(t, err)
		require.Equal(t, expected, string(b))
	}
	// Delete the queue
	n, err = conn.QueueDelete(ctx, q, false, false)
	require.NoError(t, err)
	require.Zero(t, n)
}
